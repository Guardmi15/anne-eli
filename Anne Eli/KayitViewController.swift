//
//  KayitViewController.swift
//  Anne Eli
//
//  Created by Batuhan GÖKTEPE on 6.07.2017.
//  Copyright © 2017 Batuhan GÖKTEPE. All rights reserved.
//

import UIKit
import Firebase

class KayitViewController: UIViewController {

    @IBOutlet weak var loginWithFace: UIButton!
    @IBOutlet weak var anneEli: UILabel!
    @IBOutlet weak var kayitSayfasi: UILabel!
    @IBOutlet weak var kayitoloutlet: UIButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var passAgainField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        loginWithFace.layer.cornerRadius = 15.0
        kayitoloutlet.layer.cornerRadius = 15.0
        
        func iPhoneScreenSizes(){
            let bounds = UIScreen.main.bounds
            let height = bounds.size.height
            
            switch height {
            case 480.0:
                print("iPhone 3,4")
            case 568.0:
                print("iPhone5")
                anneEli.font = anneEli.font.withSize(55)
                kayitSayfasi.font = kayitSayfasi.font.withSize(45)
                loginWithFace.titleLabel?.font = loginWithFace.titleLabel?.font.withSize(25)
                
                
                
            case 667.0:
                print("iPhone 6")
                print("iPhone 6+")
                
                
                
            case 736.0:
                print("iPhone 6+")
                
                
                
                
            default:
                print("not an iPhone")
                
            }
            
            
        }
        iPhoneScreenSizes()



    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func kayitOl(_ sender: AnyObject) {
        
        Auth.auth().createUser(withEmail: emailField.text!, password: passField.text!) { (user, error) in
            
        }
            
            
    }

    @IBAction func faceKayit(_ sender: AnyObject) {
    }
  

}
